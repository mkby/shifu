use crate::toml_handler::HandleConfig;
use regex::Regex;
use std::fs::File;
use std::io::prelude::*;

pub fn use_prefix(input_str: String) -> String {
    let mut file = File::open("/etc/shifu/config.toml").expect("could not open config file");
    let mut content = String::new();
    file.read_to_string(&mut content)
        .expect("Unable to read config file");

    let config: HandleConfig =
        toml::from_str(content.as_str()).expect("Your config file has an Error");
    //-DCMAKE_INSTALL_PREFIX=/usr
    let prefix = config.configuration.prefix.as_str();

    let fixconfigure = format!("./configure --prefix={}$1", prefix);
    let fixmeson = format!("meson --prefix={}$1", prefix);
    let fixcmake = format!("-DCMAKE_INSTALL_PREFIX={}$1", prefix);

    let mesonregex = Regex::new(r"(?m)^meson(\s)").unwrap();
    let configureregex = Regex::new(r"(?m)^./configure --prefix=/usr(\s)").unwrap();

    let didcmake = &input_str.replace("-DCMAKE_INSTALL_PREFIX=/usr", fixcmake.as_str());
    let didmeson = mesonregex.replace_all(didcmake, fixmeson).to_string();
    let finalresult = configureregex
        .replace_all(&didmeson, fixconfigure)
        .to_string();
    finalresult
}
