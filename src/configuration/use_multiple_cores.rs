use crate::toml_handler::HandleConfig;
use regex::Regex;
use std::fs::File;
use std::io::prelude::*;
use toml;

pub fn use_multi_core(input_str: String) -> String {
    print!("entering function");
    let mut file = File::open("/etc/shifu/config.toml").expect("could not open config file");

    let mut content = String::new();
    file.read_to_string(&mut content)
        .expect("Unable to read config file");
    let str = content;

    let makere = Regex::new(r"(?m)^make(\s)").unwrap();
    let ninjare = Regex::new(r"(?m)^ninja(\s)").unwrap();

    let config: HandleConfig = toml::from_str(str.as_str()).expect("Your config file has an Error");

    println!("Applying your {} cores..", config.hardware.core_count);

    let cores = format!("-j{}", config.hardware.core_count);
    let fixninja = format!("ninja -j{}$1", config.hardware.core_count);
    let fixmake = format!("make -j{}$1", config.hardware.core_count);

    let use_cores = &input_str.replace("-j<N>", cores.as_str());
    let didninja = ninjare.replace_all(use_cores, fixninja).to_string();

    let finished = makere.replace_all(&didninja, fixmake).to_string();

    finished

    //return file;
}
