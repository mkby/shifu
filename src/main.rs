use std::{env::args, process::exit};

use configuration::*;
use operations::errors::not_enough_args;
use operations::*;
mod configuration;
mod operations;
use crate::build::build;
use crate::handle_input::handle_install_types;
use crate::sideload::sideload;
use crate::system_functions::check_if_root;
/// Handle argument parsing
fn handle_args() {
    let mut args = args();

    args.next();

    match args
        .next()
        .unwrap_or_else(|| {
            not_enough_args();
            unreachable!()
        })
        .replace('-', "")
        .as_ref()
    {
        "install" | "i" => {
            if args.len() < 1 {
                not_enough_args()
            }
            check_if_root();
            let args: Vec<String> = std::env::args().skip(2).collect();
            handle_install_types(args);
        }
        "build" | "b" => {
            if args.len() < 1 {
                not_enough_args()
            }
            check_if_root();

            for package in args {
                build(&package);
            }
        }

        "sideload" | "s" => {
            if args.len() < 2 {
                not_enough_args()
            }
            check_if_root();
            let args: Vec<String> = std::env::args().skip(2).collect();

            sideload(args);
        }

        "help" | "h" => {
            println!(
                r"shifu <OPERATION> [ARGUMENTS...]

    help    (h): Show this help message
    install (i): Install one or more packages
    build   (b): Creat binaries for a given package
"
            )
        }

        v => {
            println!("Operation not recognized: {}\nTry using 'shifu help'", v);
            exit(1)
        }
    }
}

fn main() {
    handle_args()
}
