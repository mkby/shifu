use crate::download::async_download;
use crate::errors::bin_not_exist;
use toml::Value;

pub fn sideload(mut input: Vec<String>) {
    let veccount = input.len();
    let destination = input[veccount - 1].clone();
    input.remove(veccount - 1);
    println!("{}", destination);
    let urls = get_url_for_bin(&input);
    async_download(urls, input, destination);
    println!("func ran");
}
fn get_url_for_bin(packages: &Vec<String>) -> Vec<String> {
    let response =
        reqwest::blocking::get("https://gitlab.com/curtainos/bin-repo/-/raw/main/index.toml")
            .unwrap_or_else(|e| {
                eprintln!("Unknown error: {}", e);
                std::process::exit(1);
            });
    let index = response.text().unwrap();
    let toml_index = index
        .as_str()
        .parse::<Value>()
        .ok()
        .and_then(|r| match r {
            Value::Table(table) => Some(table),
            _ => None,
        })
        .unwrap_or_default();
    let mut urls = Vec::new();
    for i in packages {
        if toml_index.get(i) == None {
            bin_not_exist(i.to_string());
        }
        urls.push(format!(
            "https://gitlab.com/curtainos/bin-repo/-/raw/main/{}-{}.curtain.zlib",
            i,
            toml_index[i].as_str().unwrap()
        ));
    }

    urls
}
