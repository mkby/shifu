use crate::use_prefix::use_prefix;
use crate::{replace_placeholders::replace_placeholders, use_multiple_cores::use_multi_core};
use std::env;
use std::fs;
use std::path::Path;
use std::process::Command;

pub fn run(string_to_run: String, path: String) {
    let startroot = path;
    let mut root = Path::new(&startroot);
    fs::create_dir_all(&root).unwrap();

    println!("{}", root.display());
    let mut _newroot = String::new();

    for line in string_to_run.lines() {
        env::set_current_dir(&root).unwrap();
        if line.trim().is_empty() {
            continue;
        } else if line.contains('=') || line.starts_with("./") || line.starts_with("if") {
            Command::new("zsh")
                .arg("-c")
                .arg(line)
                .spawn()
                .unwrap()
                .wait()
                .unwrap();

            continue;
        } else if line.contains("cd ..") {
            root = Path::new(root.parent().unwrap());

            continue;
        } else if line.contains("cd") {
            let cd_command: Vec<&str> = line.split_whitespace().collect();
            _newroot = format!("{}/{}", root.display(), cd_command[1]);
            root = Path::new(&_newroot);

            continue;
        } else {
            let args: Vec<&str> = line.split_whitespace().collect();
            Command::new(&args[0])
                .args(&args[1..])
                .spawn()
                .unwrap()
                .wait()
                .unwrap();
            continue;
        }
    }
}
pub fn mutate_buildfile(buildfile: &String) -> String {
    println!("{}", buildfile);

    let firstedit = replace_placeholders(buildfile);
    let secondedit = use_multi_core(firstedit);
    use_prefix(secondedit)
}
