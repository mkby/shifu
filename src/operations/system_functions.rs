use nix::unistd::Uid;
use std::process::exit;

pub fn check_if_root() {
    if !Uid::effective().is_root() {
        println!("You must run this executable with root permissions");
        exit(1);
    }
}
