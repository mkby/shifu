use crate::download::get_buildfile;
use crate::errors::invalid_toml_format;
use crate::install::install;
use crate::install_bin::bin_install;
use crate::install_git::git_install;
use crate::install_source::source_install;
use crate::manage::check_if_toml_exists;
use crate::manage_bin::check_if_bin_exists;
use crate::toml_handler::HandleBuildfile;
use std::process::exit;

//if user inputs a bin specifically
pub fn handle_install_types(packages: Vec<String>) {
    println!("{:?}", packages);
    for package_name in packages {
        if package_name.ends_with("-bin") {
            let mut nombre: String = package_name.to_string();
            for i in 1..5 {
                //prints a countdown
                println!("{}", 4 - i);
                //removed the -bin part
                nombre.pop();
            }

            let bincheck = check_if_bin_exists(&nombre);

            if &bincheck == "nope, this binary does not exist" {
                eprintln!("binary {} not found", nombre);
                exit(1);
            } else {
                bin_install(&nombre, &bincheck, "/");
                continue;
            }
        } else if package_name.ends_with("-git") {
            let mut nombre: String = package_name.to_string();
            for i in 1..5 {
                //prints a countdown
                println!("{}", 4 - i);
                //removed the -git part
                nombre.pop();
            }

            if check_if_toml_exists(format!("{}-git", nombre).as_str()) {
                git_install(&nombre);
                continue;
            }
        } else if package_name.ends_with("-source") {
            let mut nombre: String = package_name.to_string();
            for i in 1..8 {
                //prints a countdown
                println!("{}", 7 - i);
                //removed the -source part
                nombre.pop();
            }

            let script = get_buildfile(&nombre);
            if !script.trim().is_empty() {
                let config: HandleBuildfile =
                    toml::from_str(script.as_str()).unwrap_or_else(|_| {
                        invalid_toml_format();
                        exit(1);
                    });
                source_install(config, script);
                continue;
            }
        } else {
            install(&package_name);
        }
    }
}
