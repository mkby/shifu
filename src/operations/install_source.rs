use crate::download::download;
use crate::run::*;
use crate::toml_handler::HandleBuildfile;

pub fn source_install(config: HandleBuildfile, install_script: String) {
    println!(
        "Welcome, I will now install {} for you",
        config.package.name
    );
    println!(
        "It depends on {:?} to function properly and needs {:?} to build.",
        config.dependencies.depends, config.dependencies.build
    );

    let applied_script = mutate_buildfile(&install_script);
    print!("{}", applied_script);

    let config: HandleBuildfile = toml::from_str(&applied_script).unwrap();

    let destdir = format!("/etc/shifu/cache/source/{}", config.package.name);

    let buildfile = [
        config.install.installation,
        config.install.sysvinit.unwrap_or_default(),
    ]
    .join("\n");
    download(
        &config.dependencies.source,
        format!("{}/{}.tar", destdir, config.package.name).as_str(),
    );
    std::process::Command::new("tar")
        .arg("-xf")
        .arg(format!("{}/{}.tar", destdir, config.package.name))
        .arg("-C")
        .arg(&destdir)
        .spawn()
        .unwrap()
        .wait()
        .unwrap();

    run(
        buildfile,
        format!("{}/{}-{}", destdir, config.package.name, config.package.ver),
    );
}
