use crate::download::get_buildfile_or_error;
use crate::manage_git::clone;
use crate::run::*;
use crate::toml_handler::HandleBuildfile;

pub fn git_install(name: &str) {
    eprintln!("can't install git yet{}", name);
    let startroot = format!("/etc/shifu/cache/git/{}", name);
    let script = get_buildfile_or_error(format!("{}-git", name).as_str());
    let buildfile = mutate_buildfile(&script);

    let config: HandleBuildfile = toml::from_str(buildfile.as_str()).expect("toml issue");
    clone(config.dependencies.source, &startroot);

    let buildfile = [
        config.install.installation,
        config.install.sysvinit.unwrap_or_default(),
    ]
    .join("\n");

    run(buildfile, format!("/etc/shifu/cache/git/{}", name));
}
