use reqwest::StatusCode;
use std::process::exit;

pub fn check_if_toml_exists(name: &str) -> bool {
    let response = reqwest::blocking::get(format!(
        "https://codeberg.org/CurtainOS/repo/raw/branch/main/{}.toml",
        name
    ))
    .unwrap_or_else(|e| {
        eprintln!("Unknown error: {}", e);
        exit(1)
    });

    match response.status() {
        StatusCode::OK => true,
        _e => false,
    }
}
