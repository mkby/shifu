use crate::archive::create_tarball;
use crate::binfo::create_binary_info;
use crate::compression::copy_file_to_zstd;
use crate::destdir::destdir;
use crate::download::{download, get_buildfile_or_error};
use crate::hash::get_hash_for_dir;
use crate::questions::yes_or_no;
use crate::replace::*;
use crate::run::*;
use crate::toml_handler::HandleBuildfile;
use colored::Colorize;
use question::Answer;
use regex::Regex;
use std::fs;
use std::fs::File;
use std::io::Write;
use std::process::exit;
use std::process::Command;

pub fn build(package_name: &str) {
    let install_script = get_buildfile_or_error(package_name);
    println!("{}", install_script);
    let toml_str = install_script.as_str();
    let config: HandleBuildfile = toml::from_str(toml_str).unwrap();

    println!(
        "To start building {} v.{} you need {:#?}, {:#?} and {:#?}. \n \n {} if these packages are not on your host, your binary build will be faulty and not usable for production(assuming it will even work)
        ",
        config.package.name.green(), config.package.ver.yellow(), config.dependencies.depends, config.dependencies.build, config.dependencies.opt, "WARNING: ".to_string().red()
    );
    if yes_or_no(
        "ARE YOU SURE ALL OF THESE PACKAGES ARE INSTALLED???",
        Answer::YES,
    ) {
        println!("proceeding");
    } else {
        eprintln!("you need all of these packages installed, have a great day, see you soon :)");
        exit(0)
    }

    // this regex is used to allocate all "/" that do not have any text before them
    //for example /usr/bin could become test/usr/bin
    regex::escape("/");
    let first_slash_regex = Regex::new(r#"(\s)/"#).unwrap();
    let build_name = format!("{}-{}", config.package.name, config.package.ver);
    println!("{}", build_name);
    let build_dir = format!(
        "/etc/shifu/cache/tmp/{}/to/{}",
        config.package.name, build_name
    );

    let fixed_to_right_dir = first_slash_regex
        .replace_all(&install_script, format!(" {}/", build_dir))
        .to_string();
    println!("{}", fixed_to_right_dir);

    //create basic filesystem to avoid errors when a package uses cp
    fs::create_dir_all(build_dir.as_str()).unwrap();

    let edited = destdir(&fixed_to_right_dir, &build_dir);
    let edited1 = regex_replace(&edited, "(?m)^cp(\\s)", "install -d ");
    let finished = mutate_buildfile(&edited1);
    //temporarily dissabled because of interference with the ninja install
    // let finished1 = regex_replace(&replaced_cp, "(?m)^install(\\s)", "install -d ");

    //make a toml out of it to pass right build args
    let fincfg: HandleBuildfile = toml::from_str(&finished).unwrap();

    let buildfile = [
        fincfg.install.installation,
        fincfg.install.sysvinit.unwrap_or_default(),
    ]
    .join("\n");

    let downdir = format!("/etc/shifu/cache/tmp/{}/from", config.package.name);
    download(
        &fincfg.dependencies.source,
        &format!("{}/{}.tar", downdir, config.package.name),
    );
    println!("{}", buildfile);
    Command::new("tar")
        .arg("-xf")
        .arg(format!("{}/{}.tar", downdir, config.package.name))
        .arg("-C")
        .arg(&downdir)
        .spawn()
        .unwrap()
        .wait()
        .unwrap();

    let path = std::env::current_dir().unwrap();
    run(
        buildfile,
        format!("{}/{}-{}", downdir, config.package.name, config.package.ver),
    );
    std::env::set_current_dir(path).unwrap();

    let info_info = create_binary_info(toml_str.to_string());
    let mut info_file = File::create(format!("{}/info.toml", &build_dir)).unwrap();
    info_file.write_all(info_info.as_bytes()).unwrap();

    // shorten url

    let hash = get_hash_for_dir(&build_dir);
    let to_remove = format!("/etc/shifu/cache/tmp/{}/", &build_name);
    let hash1 = hash.replace(&to_remove, "");
    //let finished_hash = hash1.replace("hash.txt*", "");
    //remove last one because it is faulty
    let wronghash = hash1.lines().last().unwrap();
    let finished_hash = hash1.replace(wronghash, "");

    let mut file = File::create(format!("{}/hash.txt", &build_dir)).unwrap();
    file.write_all(finished_hash.as_bytes()).unwrap();

    println!("{}", finished_hash);
    create_tarball(
        fincfg.package.name.to_string(),
        fincfg.package.ver,
        &build_dir,
    );
    let filename = format!("{}.curtain", build_name);
    copy_file_to_zstd(&filename);
    fs::remove_file(filename).unwrap();
}
