use regex::Regex;

pub fn regex_replace(file: &str, regex: &str, replace_with: &str) -> String {
    let re = Regex::new(regex).unwrap();
    let result = re.replace_all(file, &replace_with.to_string()).to_string();
    result
}
