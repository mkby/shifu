use futures::stream;
use futures::StreamExt;
use indicatif::{MultiProgress, ProgressBar, ProgressStyle};
use reqwest;
use reqwest::Client;
use reqwest::StatusCode;
use std::cell::RefCell;
use std::cmp::min;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;
use std::process::exit;
use tokio;
use tokio::time::{sleep, Duration};

pub fn get_buildfile_or_error(package_name: &str) -> String {
    let mut url = "https://codeberg.org/CurtainOS/repo/raw/branch/main/".to_string();
    url.push_str(package_name);
    url.push_str(".toml");
    //send request to the url
    let response = reqwest::blocking::get(url).unwrap_or_else(|e| {
        println!("Unknown error: {}", e);
        exit(1);
    });

    match response.status() {
        StatusCode::OK => response.text().unwrap(),
        StatusCode::NOT_FOUND => {
            println!("Package '{}' not found", package_name);
            exit(1);
        }

        v => {
            println!("Unknown error. Request status code: {}", v);
            exit(1);
        }
    }
}

pub fn get_buildfile(package_name: &str) -> String {
    let mut url = "https://codeberg.org/CurtainOS/repo/raw/branch/main/".to_string();
    url.push_str(package_name);
    url.push_str(".toml");
    //send request to the url
    let response = reqwest::blocking::get(url).unwrap_or_else(|e| {
        println!("Unknown error: {}", e);
        exit(1);
    });

    response.text().unwrap()
}

pub fn download(url: &str, destination: &str) {
    println!("downloading {} ...", url);
    let path = std::path::Path::new(destination);
    let parents = path.parent().unwrap();
    std::fs::create_dir_all(parents).unwrap();
    //extra client is needed because a timeout is fatal for bigger packages so we disable it
    let client = reqwest::blocking::Client::builder()
        .timeout(None)
        .build()
        .unwrap();
    //here we start the reqwest based on the custom builder
    let response = client.get(url).send().unwrap_or_else(|e| {
        println!("Unknown error: {}", e);
        exit(1);
    });
    //match the response body for common HTML status
    match response.status() {
        StatusCode::OK => {
            let mut file = File::create(destination).unwrap();

            let answer = response.bytes().unwrap(); //keep as bytes because else the packages will be faulty
            file.write_all(&answer).unwrap(); // write to file
        }
        StatusCode::NOT_FOUND => {
            println!("Could not find this file at given url");
            exit(1);
        }

        v => {
            println!("Unknown error. Request status code: {}", v);
            exit(1);
        }
    }
}
#[tokio::main]
pub async fn async_download(urls: Vec<String>, names: Vec<String>, destination: String) {
    let number_of_downloads = names.len();
    let concurrent_reqwests: usize = number_of_downloads;
    let client = reqwest::Client::new(); //create new client
    let bodies = stream::iter(urls)
        .map(|url| {
            let client = &client;
            let destination = &destination;
            async move {
                let resp = client.get(&url).send().await.unwrap();

                let total_size = resp
                    .content_length()
                    .ok_or(format!("Failed to get content length from '{}'", &url))
                    .unwrap();
                let final_destination =
                    format!("{}/{}", destination, resp.url().path_segments().unwrap().last().unwrap());
                    println!("hi: {}", final_destination);

                //define progress bar
                let pb = ProgressBar::new(total_size);
                pb.set_style(ProgressStyle::default_bar()
       .template("{msg}\n{spinner:.green} [{elapsed_precise}] [{wide_bar:.cyan/blue}] {bytes}/{total_bytes} ({bytes_per_sec}, {eta})"));

                pb.set_message(format!("Downloading {} to {}", url, final_destination));

                let output_path = Path::new(&final_destination);

                std::fs::create_dir_all(output_path.parent().unwrap()).unwrap();

                let mut file = File::create(&final_destination).unwrap();
                let mut downloaded: u64 = 0;
                //let returnable = resp.bytes().await.unwrap().to_owned().cop();
                let mut stream = resp.bytes_stream();

                while let Some(item) = stream.next().await {
                    let chunk = item.unwrap();
                    file.write_all(&chunk).unwrap();
                    let new = min(downloaded + (chunk.len() as u64), total_size);
                    downloaded = new;
                    pb.set_position(new);
                }
                pb.finish_with_message(format!("Downloaded {} to {}", url, final_destination));
            }
        })
        .buffer_unordered(concurrent_reqwests);

    bodies
        .for_each(|b| async move {
            //wait for a few milliseconds to avoid progressbar overloading
            sleep(Duration::from_millis(100)).await
        })
        .await;
}
