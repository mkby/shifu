use crate::toml_handler::{BinDependencies, CreateInfo, HandleBuildfile, Info};
use toml;

pub fn create_binary_info(tomlstr: String) -> String {
    let config: HandleBuildfile = toml::from_str(&tomlstr).unwrap();

    let binconfig = CreateInfo {
        info: Info {
            name: config.package.name,
            version: config.package.ver,
            description: config.package.des,
        },
        dependencies: BinDependencies {
            needed: config.dependencies.depends,
            optional: config.dependencies.opt,
        },
    };
    toml::to_string(&binconfig).unwrap()
}
