use flate2::write::ZlibEncoder;
use flate2::Compression;
use std::fs::File;
use std::io::prelude::*;
use std::io::Write;

pub fn copy_file_to_zstd(path: &String) {
    println!("{}", path);
    let input = File::open(&path).unwrap();
    let mut output = File::create(format!("{}.zlib", &path)).unwrap();

    let mut compressor = ZlibEncoder::new(input, Compression::fast());
    let mut result = Vec::new();
    compressor.read_to_end(&mut result).unwrap();
    output.write_all(&result).unwrap();
}
