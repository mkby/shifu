use std::process::exit;

// Gonna be used in more places eventually
/// "Not enough arguments" error
pub fn not_enough_args() {
    eprintln!("Not enough arguments supplied (at least two requred)\nTry using 'shifu help'");
    exit(1);
}
pub fn invalid_toml_format() {
    eprintln!("The toml file was invalid, please contact the provider");
    exit(1);
}
pub fn bin_not_exist(e: String) {
    eprintln!("The binary file {} does not exist. Please check if this is the correct name and weather the right repositories are enabled.", e);
    exit(1);
}
